from django.urls import path
from . import views

app_name = 'posts'
urlpatterns = [
    path('', views.PostListView.as_view(), name='index'),
    path('category/', views.CategoryListView.as_view(), name='categorias'),
    path('create/', views.PostCreateView.as_view(), name='create'),
    path('<pk>/', views.PostDetailView.as_view(), name='detail'),
    path('<int:post_id>/comment/', views.create_comment, name='comment'),
    path('update/<pk>/', views.PostUpdateView.as_view(), name='update'),
    path('delete/<pk>/', views.PostDeleteView.as_view(), name='delete'),
    path('category/<str:cats>/', views.CategoryView, name='category'),

]