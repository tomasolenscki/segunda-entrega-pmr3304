from email.policy import default
from django.db import models
from django.conf import settings
from django.utils.timezone import now


class Category(models.Model):
    nome = models.CharField(max_length=255)
    descricao = models.CharField(max_length=255)

    def __str__(self):
        return f'"{self.nome}" - {self.descricao}'

class Post(models.Model):
    titulo = models.CharField(max_length=255)
    conteudo = models.TextField()
    data = models.DateTimeField(default=now, blank=True)
    category = models.ManyToManyField(Category)
    # author = models.ForeignKey(settings.AUTH_USER_MODEL,
                            #    on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.titulo} ({self.data})'


class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    data = models.DateTimeField(default=now, blank=True)
    likes = models.IntegerField(default=0)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-data']

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'


