from django import forms
from django.forms import ModelForm
from .models import Post, Comment, Category


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'titulo',
            'conteudo',
            'data',
        ]
        labels = {
            'titulo': 'Título',
            'conteudo': 'Conteúdo',
            'data': 'Data',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
            'data',
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Resenha',
            'data': 'Data',
        }

