from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Post, Comment, Category
from .forms import PostForm, CommentForm
from django.views import generic


class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'posts/detail.html'
    allow_empty = False


# def detail_post(request, post_id):
#     post = get_object_or_404(Post, pk=post_id)
#     context = {'post': post}
#     return render(request, 'posts/detail.html', context)


class PostListView(generic.ListView):
    model = Post
    form_class = PostForm
    template_name = 'posts/index.html'
    sucess_url = '/posts/'
    
# def list_posts(request):
#     post_list = Post.objects.all()
#     context = {"post_list": post_list}
#     return render(request, 'posts/index.html', context)


class PostCreateView(generic.CreateView):
    model = Post
    form_class = PostForm
    template_name = 'posts/create.html'
    success_url = '/posts/'


# def create_post(request):

#     if request.method == 'POST':
#         form = PostForm(request.POST)
#         if form.is_valid():

#             post_titulo = request.POST['titulo']
#             post_conteudo = request.POST['conteudo']
#             post_data = request.POST['data']

#             post = Post(titulo = post_titulo,
#                         conteudo = post_conteudo,
#                         data = post_data)
#             post.save()

#             return HttpResponseRedirect(reverse('posts:detail', args=(post.id, )))
#     else:
#         form = PostForm()
#     context = {'form' : form}
#     return render(request, 'posts/create.html', context)

class PostUpdateView(generic.UpdateView):
    model = Post
    form_class = PostForm
    template_name = 'posts/update.html'
    success_url = '/posts/'

# def update_post(request, post_id):
#     post = get_object_or_404(Post, pk=post_id)

#     if request.method == "POST":

#         form = PostForm(request.POST)

#         if form.is_valid():

#             post.titulo = request.POST['titulo']
#             post.conteudo = request.POST['conteudo']
#             post.data = request.POST['data']
#             post.save()

#             return HttpResponseRedirect(reverse('posts:detail', args=(post.id, )))
    
#     else:
#         form = PostForm(
#             initial = {
#             'titulo': post.titulo,
#             'conteudo': post.conteudo,
#             'data': post.data              
#             })

#     context = {'post' : post, 'form' : form}
#     return render(request, 'posts/update.html', context)


class PostDeleteView(generic.DeleteView):
    model = Post
    success_url = '/posts/'
    template_name = 'posts/delete.html'
    allow_empty = False

# def delete_post(request, post_id):
#     post = get_object_or_404(Post, pk=post_id)

#     if request.method == "POST":
#         post.delete()
#         return HttpResponseRedirect(reverse('posts:index'))

#     context = {'post': post}
#     return render(request, 'posts/delete.html', context)

def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            post=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('posts:detail', args=(post_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'posts/comment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'posts/categorias.html'

def CategoryView(request, cats):
    category = get_object_or_404(Category, nome = cats)
    return render(request, 'posts/categories.html', { 'category' : category })

